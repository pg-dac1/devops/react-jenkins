import axios from "axios";
import { useState } from "react";
import { toast } from "react-toastify";
import { useNavigate } from "react-router";

const Insert = () =>{

    const [id, setId] = useState('')
    const [name, setName] = useState('')
    const [model, setModel] = useState('')
    const [price, setPrice] = useState('')
    const [carColor, setCarColor] = useState('')


    const navigate = useNavigate();
    const insertCar = () =>{
        if(name.length===0 && model.length===0 && carColor.length === 0 ){
            toast.warning("enter all fields");
        }else{

            const body = {
                id, name, model, price, carColor
            }
            const url = `http://65.0.127.115:4000/car/insert`

            axios.post(url,body).then((response)=>{
                const result = response.data
                if(result.status === 'success'){
                    navigate('/admin')
                    toast.success("Data inserted ")
                }else(
                    toast.warning("Insertion failed")
                )
            });
        }
    }

    return (
        <div>
            
            <div className="row">
                <div className="col"></div>

                <div className="col">
                    <div className="form">

                        <div className="row">
                            <div className="col">
                                <div className="mb-3">
                                    <label class="form-label">Id</label>
                                    <input type="number" className="form-control" aria-describedby="emailHelp"
                                    onChange={(e)=>setId(e.target.value)}/>
                                </div>
                            </div>

                            <div className="col">
                                <div className="mb-3">
                                    <label className="form-label">Name</label>
                                    <input type="text" className="form-control" aria-describedby="emailHelp"
                                    onChange={(e)=>setName(e.target.value)}/>
                                </div>
                            </div>
                        </div>
                        
                        <div className="mb-3">
                            <label className="form-label">Model</label>
                            <input type="text" className="form-control" aria-describedby="emailHelp"
                            onChange={(e)=>setModel(e.target.value)}/>
                        </div>

                        <div className="mb-3">
                            <label className="form-label">Price</label>
                            <input type="number" className="form-control" aria-describedby="emailHelp"
                            onChange={(e)=>setPrice(e.target.value)}/>
                        </div>

                        <div className="mb-3">
                            <label className="form-label">Car color</label>
                            <input type="tel" className="form-control" aria-describedby="emailHelp"
                            onChange={(e)=>setCarColor(e.target.value)}/>
                        </div>

                        <button className="btn-sm btn-primary" onClick={insertCar} > Add Car </button>
                    </div>
                </div>

                <div className="col"></div>
            </div>

        </div>
    )
}

export default Insert;
