import { useState } from "react";
import axios from 'axios'
import { toast } from "react-toastify";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";


const Signin = () =>{

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const navigate = useNavigate();

    const signinUser = () =>{
        if(email.length===0){
            toast.warning("please enter Email")
        }else if(password.length===0){
            toast.warning("please enter Password")
        }else{

            const body = {email, password}
            const url = `http://65.0.127.115:4000/user/authenticate`

            axios.post(url,body).then((response)=>{
                const result = response.data;
                if (result['status']==='success' && result[0] !== null) {
                    navigate('/admin');
                    toast.success("Signed Successfully ");
                }else{
                    toast.error("Login failed")
                }
            });
        }
        console.log(email,password);

    }

    return(
        <div>
         <div className="col"></div>
            <div className="col">
                <div className="form">
                    <div className="mb-3">
                        <label htmlFor="" className="label-control"  >
                            Email address
                        </label>
                        <input onChange={(e) => { setEmail(e.target.value) }} type="text" className="form-control" />
                    </div>

                    <div className="mb-3">
                        <label htmlFor="" className="label-control">
                            Password
                        </label>
                        <input onChange={(e) => {setPassword(e.target.value)}} type="password" className="form-control" />
                    </div>

                    <div className="mb-3">                    
                        <button onClick={signinUser} className="btn btn-primary">
                            Signin
                        </button>
                    </div>
                </div>
            </div>
            <div className="col"></div>    
             
        </div>
    );
}

export default Signin