import axios from "axios";
import { useEffect } from "react";
import Car from "../../Components/Car";
import { useState } from "react";

const Getall = () =>{

    const [result, setResult] = useState([])

    const GetCars = () =>{
        const url = `http://65.0.127.115:4000/car/get-all`

        axios.get(url).then((response)=>{
            const result = response.data;
            setResult(result.data);
        });
    }

    useEffect(()=>{GetCars()},[])
    
    return (
        <div>
            {result.map( (c) => {return <Car res= {c} > </Car>  }  )}
        </div>
    )

}

export default Getall;