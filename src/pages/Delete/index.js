import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";

const DeleteCar = () => {

    const [id, setId] = useState(0)

    const navigate = useNavigate();


    const deleteCar = () => {

        const url = `http://65.0.127.115:4000/car/delete/${id}`

        axios.delete(url).then((response) => {

            const result = response.data
            if (result.status === 'success') {
                navigate('/admin')
                toast.success("Deleted Successfully")
            } else (
                toast.warning("Delete failed")
            )
        });
    }


    return (
        <div className="container">
            <div className="row">
                <div className="col">
                </div>

                <div className="col">
                    <h2 >Enter Car Id </h2>
                    <input type="number" onChange={(e) => { setId(e.target.value) }} />
                    <button type="button" onClick={deleteCar} class="btn btn-danger">Delete</button>

                </div>

                <div className="col">
                </div>
            </div>
        </div>
    )
}

export default DeleteCar;